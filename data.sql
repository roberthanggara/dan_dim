-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 18 Jun 2019 pada 04.16
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `data`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `datadiri1`
--

CREATE TABLE IF NOT EXISTS `datadiri1` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(40) NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `jenkel` varchar(45) NOT NULL,
  `hobi` varchar(200) NOT NULL,
  `jml_sdr` int(3) NOT NULL,
  `ortu` varchar(20) NOT NULL,
  `foto` varchar(60) NOT NULL,
  `komentar` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data untuk tabel `datadiri1`
--

INSERT INTO `datadiri1` (`id`, `nama`, `alamat`, `email`, `no_telp`, `jenkel`, `hobi`, `jml_sdr`, `ortu`, `foto`, `komentar`) VALUES
(2, 'Agus Bagus', 'malang', 'firmanhdwlucyus@gmail.com', '081334435248', 'perempuan', 'makan', 3, 'complete', 'neko.jpg', 'kmkmkmlknklnkkm'),
(3, 'Wijayanto', 'tuban', 'firmanhdwlucyus@gmail.com', '081334435248', 'perempuan', 'menyanyi', 3, 'singel parent', 'gundam.jpg', 'klklkplmmjnjkjk'),
(32, 'a', 'a', 'ayra7nugraha@gmail.com', 'a', 'Laki-laki', 'a:3:{i:0;s:11:"Jalan-jalan";i:1;s:5:"Makan";i:2;s:5:"Tidur";}', 7, 'complete', 'neko.jpg', ',b.m.kn.fsmal;zmfk.'),
(35, '', '', '', '', 'Laki-laki', 'a:1:{i:0;s:11:"Jalan-jalan";}', 0, 'singel parent', 'gundam.jpg', ''),
(36, '', '', 'ayra7nugraha@gmail.com', '', 'Laki-laki', 'a:2:{i:0;s:11:"Jalan-jalan";i:1;s:5:"Makan";}', 2, '2', '', 'hfnvjhfvnbvvbnvbv'),
(37, '', 'gbv b', 'ayra7nugraha@gmail.com', '', 'Laki-laki', 'a:2:{i:0;s:11:"Jalan-jalan";i:1;s:5:"Makan";}', 5, '1', '', 'fvcvcvbnb');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
